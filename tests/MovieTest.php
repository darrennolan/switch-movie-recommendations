<?php

use Carbon\Carbon;
use App\Movie;

class MovieTest extends TestCase
{
    // public function setUp()
    // {
    // }

    public function tearDown()
    {
        Mockery::close();
    }

    /**
     * @dataProvider hasGenresProvider
     */
    public function testHasGenre($allGenres, $searchedGenre, $expected)
    {
        $mockMovie = new stdClass();
        $mockMovie->name = '';
        $mockMovie->rating = 0;
        $mockMovie->showings = [];
        $mockMovie->genres = $allGenres;

        $movie = new Movie($mockMovie);

        $this->assertEquals($movie->hasGenre($searchedGenre), $expected);
    }

    public function hasGenresProvider()
    {
        return [
            'Drama contains drama'                  => [['Drama'], 'drama', true],
            'Action & Adventure contains adventure' => [['Action & Adventure'], 'adventure', true],
            'Multiple genres match'                 => [['Drama', 'Comedy'], 'comedy', true],
            'Comedy doesnt contain drama'           => [['Comedy'], 'drama', false],
            'Multiple genres not matching'          => [['Animation', 'Comedy'], 'drama', false],
        ];
    }

    /**
     * @dataProvider getNearestShowingToTimeProvider
     */
    public function testGetNearestShowingToTime($allShowings, $searchedTime, $expected)
    {
        $mockMovie = new stdClass();
        $mockMovie->name = '';
        $mockMovie->rating = 0;
        $mockMovie->genres = [];
        $mockMovie->showings = $allShowings;

        $movie = new Movie($mockMovie);

        $parsedSearchTime = Carbon::parse($searchedTime);

        $this->assertEquals($movie->getNearestShowingToTime($parsedSearchTime), $expected);
    }

    public function getNearestShowingToTimeProvider()
    {
        return [
            '1 showing time > 30 minutes' => [['18:00:00+11:00'], '06:00:00+11:00', Carbon::parse('18:00:00+11:00')],
            'not available < 15 minutes' => [['18:00:00+11:00'], '17:45:00+11:00', false],
            'not available < 5 minutes' => [['18:00:00+11:00'], '17:55:00+11:00', false],
            'soonest returned > 30 minutes' => [['09:00:00+11:00', '18:00:00+11:00'], '06:00:00+11:00', Carbon::parse('09:00:00+11:00')]
        ];
    }
}
