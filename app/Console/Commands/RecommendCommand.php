<?php namespace App\Console\Commands;

use App\Movie;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class RecommendCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'recommend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Find recommendations for movies.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        if (empty($this->input->getOption('genre')) ||
            empty($this->input->getOption('time'))) {
            $this->error('Both genre and time are required for recommendations.');

            return;
        }

        $searchGenre = $this->option('genre');
        $searchTime = $this->option('time');

        try {
            $searchParsedTime = Carbon::parse($searchTime);
        } catch (\Exception $x) {
            $this->error('Invalid time used. Please try a format of hh:mm');

            return;
        }

        $recommendations = Movie::getMovieRecommendations($searchGenre, $searchParsedTime);

        if ($recommendations->isEmpty()) {
            $this->warn("no movie recommendations");
        } else {
            $recommendations->each(function ($movie) use ($searchParsedTime) {
                $displayShowTime = $movie
                    ->getNearestShowingToTime($searchParsedTime)
                    ->format('ga');

                $this->info("{$movie->name}, showing at {$displayShowTime}");
            });
        }

    }

    /**
     * The console options available
     *
     * @return array Contains rules for this command
     */
    protected function getOptions()
    {
        return array(
            array('genre', 'g', InputOption::VALUE_REQUIRED, 'Genre of movies to return.', null),
            array('time', 't', InputOption::VALUE_REQUIRED, 'Time to start watching, movies returned start within 30 minutes.', null)
        );
    }
}
