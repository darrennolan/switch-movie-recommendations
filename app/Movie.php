<?php
namespace App;

use Carbon\Carbon;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Collection;

class Movie
{
    /**
     * Path to movies end point.
     *
     * @var string
     */
    protected static $sourcePath = 'https://pastebin.com/raw/cVyp3McN';

    function __construct($model = null) {
        if ($model !== null) {
            $this->name     = $model->name;
            $this->rating   = $model->rating;
            $this->genres   = $model->genres;
            $this->showings = $model->showings;
        }
    }

    /**
     * Checks if this movie contains the genre, loose search.
     *
     * @param  string  $genre Genre to search for
     *
     * @return boolean        True if contains the searched genre, False otherwise
     */
    public function hasGenre($searchGenre) {
        $containsGengre = false;
        $searchGenre = strtolower($searchGenre);

        foreach($this->genres as $genre) {
            $genreLowerCase = strtolower($genre);

            if (strpos($genreLowerCase, $searchGenre) !== false) {
                $containsGengre = true;

                break;
            }
        }

        return $containsGengre;
    }

    /**
     * Finds the first nearest showing time to a given search time,
     * ensuring search time is at least 30 minutes prior.
     *
     * @param  \Carbon\Carbon        $searchTime  Time to search for
     * @return \Carbon\Carbon|false               Closest matching time as Carbon, or false if none found
     */
    public function getNearestShowingToTime(Carbon $searchTime) {
        foreach($this->showings as $showing) {
            $showingTime = Carbon::parse($showing);

            if ($searchTime->diffInMinutes($showingTime, false) >= 30) {
                return $showingTime;
            }
        }

        return false;
    }

    /**
     * Fetches our movies and converts to associative array from API.
     *
     * @return array All movies json decoded
     */
    protected static function fetchMovies()
    {
        $client = new GuzzleClient();
        $response = $client->get(static::$sourcePath);

        return json_decode($response->getBody());
    }

    /**
     * Calls fetchMovies and converts to Collection
     *
     * @return \Illuminate\Support\Collection  All Movies
     */
    public static function getMovieCollection()
    {
        return Collection::make(self::fetchMovies())
            ->map(function ($movie) {
                return new self($movie);
            });
    }

    /**
     * Given a search Genre and Time, return a list of available movies
     * that contain the genre and have a nearest showing result.  Results
     * are ordered by rating (highest to lowest)
     *
     * @param  string         $searchGenre    Genre to match movies with
     * @param  \Carbon\Carbon $searchTime     Time to match movies with
     *
     * @return \Illuminate\Support\Collection Collection of movies matching criteria
     */
    public static function getMovieRecommendations($searchGenre, $searchTime) {
        return self::getMovieCollection()
            ->filter(function ($movie) use ($searchGenre) {
                return $movie->hasGenre($searchGenre);
            })
            ->filter(function ($movie) use ($searchTime) {
                return $movie->getNearestShowingToTime($searchTime);
            })
            ->sortByDesc('rating');
    }
}
