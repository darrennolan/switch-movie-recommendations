# Switch Media Coding Task

### Getting started
$ `composer install`

### Running console command locally
$ `./artisan recommend -g {genre} -t {time}`
$ `./artisan recommend -g 'comedy' -t 12:00`
$ `./artisan recommend -g 'fantasy' -t 17:00`

Genres don't require to be wrapped in quotes, but good practice if searching for genre's containing spaces.  Time should be entered in 24 hour format.

### Running tests

$ `./vendor/bin/phpunit`

### Overview

This project aims to fetch movies from an API and help suggest recommendations based on an entered genre and time.  At this stage, both genre and times are required for recommendations to be made.

### Assumptions made.

##### Timezone Offset

This by default will use the systems timezone for parsing user time input.  Currently in Australia we're not observing DST.  However the responses coming in via our API are suggested in DST. As dates aren't returning with the show times, this app is simply calculating based on today's date.

Results are out by 1 hour. Ideally timezones returned would also contain a relative date.

##### Ordering of Showings in ASC

We're not sorting the returned showings, and expecting those to be in ascending order.  Could easily add in sorting when finding the closest showing for a movie.

##### Genre loose compare

We're currently loosely comparing genre's, case insensitive and part-matching. This allows user input to be `'fantasy'` versus `'Science Fiction & Fantasy'`.

##### Not caching Carbon parsing

Currently not caching date parsing done inside of carbon on the Movie class.  Could either do at the time of the class being created, or created and stored.  Micro optimisation, but felt the chances of parsing carbon dates when those aren't used due to not first hitting the genre filter, seemed overkill.

##### Test lacks integration around command

Currently the test suite doesn't include integration tests around the artisan command.  Given more time, I think I'd completely drop Lumen as other than some nice console/collections classes (which I can get from other packages) I'm not using any of the features here.

Lumen/Laravel used to be easier 3 years ago, I swear...

##### Some further thoughts

Should have used DI / Repo patterns for the movie class so I could test the fetching, move guzzle dependecy to outside of the movie class itself.  As a result I'm unable to test my static methods (would shouldn't be here in the first place)
